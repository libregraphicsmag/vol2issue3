Images under a CC Attribution Share-Alike license
Photo of ginger coons by herself.
Photo of Manuel Schmalstieg by Sedat Adiyaman.
Photo of Antonio Roberts by Emily Davies.
Photo of Eric Schrijver by himself.
Illustration for Friendlier forks column is based on images by Flickr user fdctsevilla.

All images in the Showcase section can be attributed to the creators mentioned therein. All are licensed CC BY-SA.

Images under other licenses

Illustration for Moving kinetic typography column is under a CC0 1.0 Universal Public Domain Dedication license and can be found at https://commons.wikimedia.org/wiki/File:Feuillet_notation.jpg
Illustration for the Libre Foundry Map by Manufactura Independente, based in this Public Domain image http://commons.wikimedia.org/wiki/File:30_Largest_Infrared_Galaxies_with_Labels.jpg.


Fonts used in the Libre type foundry Atlas:
Lil Grotesk by Bastien Sozeau, A - B Foundry, under the OFL, available at https://github.com/uplaod/LilGrotesk
Amsterdam by Jarrik Muller, Citype, under CC BY 3.0, available at http://www.citype.net/city/amsterdam
Lora by Olga Karpushina, Cyreal, under the OFL, available at http://www.cyreal.org/2012/07/lora
QumpellkaNo12 by Grzegorz Luk, Glukfonts, under the OFL, available at http://www.glukfonts.pl/font.php?font=QumpellkaNo12
Playfair Display by Claus Eggers Sørensen, Google Fonts, under the OFL, available at http://www.google.com/fonts/specimen/Playfair+Display
GFS Bodoni by George Matthiopoulos, GFS, under the OFL, available at http://www.greekfontsociety.gr/pages/en_typefaces20th.html
Alegreya Sans by Juan Pablo del Peral, Huerta Tipográfica, under the OFL, available at http://www.huertatipografica.com/fonts/alegreya-sans-ht
Antykwa Półtawskiego by Janusz Marian Nowacki, under the OFL, available at http://jmn.pl/en/antykwa-poltawskiego
Kontrapunkt Bob, Kontrapunkt, under CC BY-SA 3.0, available at http://www.kontrapunkt.com/type
Ostrich Sans by Tyler Finck, The League of Moveable Type, under the OFL, available at https://www.theleagueofmoveabletype.comostrich-sans
Unna by Jorge de Buen, Omnibus Type, under the OFL, available at http://omnibus-type.com/fonts/unna.php
Lavoir by Alex Chavot, under the OFL, available at http://openfontlibrary.org/en/font/lavoir
Reglo by Sebastien Sanfilippo, OSP Foundry, under the OFL, available at http://ospublish.constantvzw.org/foundry/reglo
Serreria Extravagante by the participants of the Stone to Spaceship workshop held at Medialab Prado in July 2012, Oxshark Fontworks, under the OFL, available at http://openfontlibrary.org/en/font/serreria-extravagante
Egypt 22 by Ivan Kostynyk, Practice Foundry, under the  OFL, available at http://practicefoundry.com/egypt22.html
Gentium by Victor Gaultney, SIL, under the OFL, available at http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=Gentium
BizMeud by Quentin Bodin and Jil Daniel, Velvetyne Type Foundry, under the OFL, available at http://velvetyne.fr/#bizmeud


General
Advertisements, with the exception of those representing Libre Graphics Magazine, are not necessarily covered by the blanket CC BY-SA license. It is best to check with the projects they represent before reusing them.
