# The Type Issue, Issue 2.3

## Index

* **Guesting/journeying**, ginger coons — *Editor's letter*
* **Type etc.**, Manuel Schmalstieg — *Guest editor's letter*
* **Typographic Vagrancy** — *Notebook*
* *New Releases*
* **Moving kinetic typography**, Antonio Roberts — *Column*
* **Friendlier forks**, Eric Schrijver — *Column*
* **Building Cyrillic fonts together**, Alexei Vanyashin — *Dispatch*
* **Forging foundries**, Ana Isabel Carvalho and Ricardo Lafuente — *Dispatch*
* **Feeling design through type**, ginger coons — *Dispatch*
* **0xA000 font family**, Øyvind Kolås — *Showcase*
* **The Screenless Office**, Brendan Howell — *Showcase*
* **Libre type foundry atlas**, Frank Adebiaye — *Showcase*
* **Pirate Party identity and PPPoster**, Samuel Rivers-Moore — *Showcase*
* *Small & Useful*
* **Best of type collections**  — *Best of*
* **Before webfonts**, Julien Deswaef — *Feature*
* **The League of Movable Type—an interview** — *Interview*
* **Capturing** — *Call for submissions*
* **Specimen**, Loraine Furter — *Feature*
* *Resources & Glossary*

## Colophon

The Type Issue — January 2015  
Issue 2.3, [Libre Graphics Magazine](http://libregraphicsmag.com)  
ISSN: 1925-1416

**Editorial Team:**
* [Ana Isabel Carvalho](http://manufacturaindependente.org)
* [ginger coons](http://adaptstudio.ca)
* [Ricardo Lafuente](http://manufacturaindependente.org)

**Copy editor:**  
Margaret Burnett  

**Publisher:**  
ginger coons  

**Community Board:**  
Dave Crossland, Louis Desjardins, Aymeric Mansoux, Alexandre Prokoudine, Femke Snelting  

**Contributors:**

* [Frank Adebiaye](http://www.fadebiaye.com/)
* [Raphaël Bastide](http://raphaelbastide.com/)
* [Julien Deswaef](http://xuv.be/)
* [Loraine Furter](http://designed.with.meteor.com/by)
* [Brendan Howell](http://wintermute.org/brendan/)
* [Øyvind Kolås](http://pippin.gimp.org/)
* [Micah Rich](https://micahrich.com/)
* [Samuel Rivers-Moore](http://samuelriversmoore.net/)
* [Antonio Roberts](http://www.hellocatfood.com/)
* [Manuel Schmalstieg](http://ms-studio.net/)
* [Eric Schrijver](http://ericschrijver.nl/)
* [Alexei Vanyashin](http://www.110design.ru/)

Printed in Brussels by [Gillis](http://gillis.be) on recycled paper.

Licensed under a [Creative Commons Attribution-ShareAlike license (CC BY-SA)](https://creativecommons.org/licenses/by-sa/3.0/).  
All content should be attributed to its individual author.  
All content without a stated author can be credited to Libre Graphics Magazine.

Write to us at enquiries@libregraphicsmag.com    
Our repositories with all source material for the magazine can be found at [https://gitlab.com/libregraphicsmag](https://gitlab.com/libregraphicsmag)

## Relevant links

* [Public announcement](http://libregraphicsmag.com/2015/02/announcing-issue-2-3-of-libre-graphics-magazine/)
* [Call for submissions](http://libregraphicsmag.com/2014/02/call-for-submissions-libre-graphics-magazine-2-3/)
* [PDF Hi-res](http://libregraphicsmag.com/files/libregraphicsmag_2.3_highquality.pdf) — download for print (123 MB)
* [PDF Low-res](http://libregraphicsmag.com/files/libregraphicsmag_2.3_lowquality.pdf) — download for screen (12 MB)
* [Git repository](https://gitlab.com/libregraphicsmag/vol2issue3/)
* [Planning wiki](http://libregraphicsmag.com/wiki/doku.php?id=volume_2)

## Repository structure

This repository contains the files used to make issue 2.3 of Libre Graphics Magazine.  
*Some of the files used in this issue can be found at [Persistent](https://gitlab.com/libregraphicsmag/persistent), a repository that contains reusable assets such as the magazine logo, columnist photos, procedures and procedures.*

* Assets folder: contains all the visual assets used  in this issue, organised by article. Here you'll find bitmap images, vector files and ads.
* Layout folder: Scribus layout document.  
* Text: one .TXT file for each article. This folder is subdivided in:
  * preedit: article received, first version, without edits.
  * firsedit: articles after the first edit.
  * secondedit: articles after the second edit.
  * final: final version of the articles published. 
* Files in the root of the repository: call for submissions for this issue; credits for the all the images used in this issue; glossary; running order and index.

## Attribution and Licenses of individual files

**Images under a CC Attribution-ShareAlike license:**
Cover, back cover and inside covers by Manufactura Independente.  
Photo of ginger coons by herself.  
Photo of Manuel Schmalstieg by Sedat Adiyaman.  
Photo of Antonio Roberts by Emily Davies.  
Photo of Eric Schrijver by himself.  
Illustration for Friendlier forks column is based on [this image](https://www.flickr.com/photos/fdctsevilla/5430867422/in/album-72157625065927544/) by Flickr user fdctsevilla.  
All images in the Showcase section can be attributed to the creators mentioned therein. All are licensed CC BY-SA.

**Images and assets under other licenses:**  
Illustration for Moving kinetic typography column is under a CC0 1.0 Universal Public Domain Dedication license and can be found [here](https://commons.wikimedia.org/wiki/File:Feuillet_notation.jpg)  
Illustration for the Libre Foundry Map by Manufactura Independente, based in [30 Largest Infrared Galaxies with Labels](http://commons.wikimedia.org/wiki/File:30_Largest_Infrared_Galaxies_with_Labels.jpg), under the Public Domain.

Fonts used in the Libre type foundry Atlas:
* [Lil Grotesk](https://github.com/uplaod/LilGrotesk) by Bastien Sozeau, A - B Foundry, under the OFL.
* [Amsterdam](http://www.citype.net/city/amsterdam) by Jarrik Muller, Citype, under CC BY-SA 3.0
* [Lora](http://www.cyreal.org/2012/07/lora) by Olga Karpushina, Cyreal type foundry, under the OFL
* [QumpellkaNo12](http://www.glukfonts.pl/font.php?font=QumpellkaNo12) by Grzegorz Luk, Glukfonts, under the OFL
* [Playfair Display](http://www.google.com/fonts/specimen/Playfair+Display) by Claus Eggers Sørensen, Google Fonts, under the OFL
* [GFS Bodoni](http://www.greekfontsociety.gr/pages/en_typefaces20th.html) by George Matthiopoulos, gfs, under the OFL
* [Alegreya Sans](http://www.huertatipografica.com/fonts/alegreya-sans-ht) by Juan Pablo del Peral, Huerta Tipográfica, under the OFL
* [Antykwa Półtawskiego](http://jmn.pl/en/antykwa-poltawskiego) by Janusz Marian Nowacki, under the OFL
* [Kontrapunkt Bob](http://www.kontrapunkt.com/type), Kontrapunkt, under CC BY-SA 3.0
* [Ostrich Sans](https://www.theleagueofmoveabletype.comostrich-sans) by Tyler Finck, The League of Moveable Type, under the OFL
* [Unna](http://omnibus-type.com/fonts/unna.php) by Jorge de Buen, Omnibus Type, under the OFL 
* [Lavoir](http://openfontlibrary.org/en/font/lavoir) by Alex Chavot, under the OFL
* [Reglo](http://ospublish.constantvzw.org/foundry/reglo) by Sebastien Sanfilippo, OSP Foundry, under the OFL
* [Serreria Extravagante](http://openfontlibrary.org/en/font/serreria-extravagante) by the participants of the [From Stone to Spaceship](http://manufacturaindependente.org/stonespaceship-workshop/) workshop (Medialab Prado, July 2012), Oxshark Fontworks, under the OFL
* [Egypt 22](http://practicefoundry.com/egypt22.html) by Ivan Kostynyk, Practice Foundry, under the OFL
* [Gentium](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=Gentium) by Victor Gaultney, SIL, under the OFL
* [BizMeud](http://velvetyne.fr/#bizmeud) by Quentin Bodin and Jil Daniel, Velvetyne Type Foundry, under the OFL

**General**  
Advertisements, with the exception of those representing Libre Graphics Magazine, are not necessarily covered by the blanket CC BY-SA license.   
It is best to check with the projects they represent before reusing them.

